<?php
/**
 * Created by PhpStorm.
 * User: kasper
 * Date: 3/12/17
 * Time: 2:22 PM
 */

namespace Drupal\tv_bot\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\Component\Utility\Unicode;

class BotController extends ControllerBase {

    private $chat_id;
    private $text;
    private $data;
    private $message_id;
    private $command;
    private $account;
    private $message_type;

    public function BotApi() {

        $content = file_get_contents("php://input");
        $update = json_decode($content, TRUE);

        if (isset($update["message"])) {

            $message = $update["message"];
            $this->text = $message['text'];
            $this->message_id = $message['message_id'];
            $this->chat_id = $message['chat']['id'];
            $user_data = $message['from'];
            $this->message_type = 'message';

        }
        elseif (isset($update['callback_query'])) {
            $this->text = $update['callback_query']['message']['text'];
            $this->data = $update['callback_query']['data'];
            $this->chat_id = $update['callback_query']['message']['chat']['id'];
            $this->message_id = $update['callback_query']['message']['message_id'];
            $message = $update['callback_query']["message"];
            $user_data = $update['callback_query']['from'];
            $this->message_type = 'callback_query';
        }


        $this->watchBot($this->message_id, serialize($update));

        $check = $this->UserIsExist();

        if (empty($check)) {

            if (isset($message['contact'])) {

                $contact = $message['contact'];

                if (isset($contact['user_id']) && $contact['user_id'] == $user_data['id']) {
                    $this->CreateUser($contact['phone_number']);
                }
            }

            $this->GetMobile();
        }
        else {

            $account = User::load(reset($check));

            if (!$account->hasRole('tv_user_bot')) {
                $message = "وضعیت شما همچنان در حال بررسی است.";
                $this->SendMessage($message, ['بررسی مجدد']);
            }

            $this->account = $account;

            if ($this->message_type == 'callback_query') {

                if ($this->data == CANCEL) {
                    $this->MainMessage(true);
                }

                $command = (array)$this->GetCommand();

                if (isset($command['command'])) {

                    $this->command = $command['command'];

                    if ($this->CheckCommand('CHANGE_THEME')) {

                        $this->SetTheme();
                        $this->ClearCommand();
                        $message = "پوسته با موفقیت تغییر کرد.";
                        $this->SendMessage($message, main_menu());

                    }

                    if ($this->CheckCommand('REMOVE_NEWS')) {

                        $this->RemoveTerm();
                        $message = "خبر با موفقیت حذف شد. خبر بعدی را برای حذف انتخاب کنید یا برای ادامه گزینه لغو را بزنید.";
                        $this->EditMessage($message, convert_to_callback($this->GetNews()), CANCEL);
                    }
                }
            }

            if ($this->message_type == 'message') {
                if ($this->CheckText(CANCEL)) {
                    $this->MainMessage(true);
                }

                if ($this->CheckText(SEND_NEWS)) {

                    $this->CreateCommand('SEND_NEWS');
                    $this->SendMessage(" متن خبر را ارسال کنید.", [], CANCEL);

                }

                if ($this->CheckText(REMOVE_NEWS)) {

                    $this->CreateCommand('REMOVE_NEWS');
                    $this->SendMessage("خبر مورد نظر را برای حذف انتخاب کنید.", convert_to_callback($this->GetNews()), CANCEL);

                }

                if ($this->CheckText(SEND_BREAKING_NEWS)) {

                    $this->CreateCommand('SEND_BREAKING_NEWS');
                    $message = " متن خبرفوری را ارسال کنید.";
                    $this->SendMessage($message);

                }

                if ($this->CheckText(REMOVE_BREAKING_NEWS)) {

                    $this->RemoveBreakingNews();
                    $message = " خبر فوری با موفقیت حذف شد.";
                    $this->SendMessage($message);

                }

                if ($this->CheckText(CHANGE_THEME)) {

                    $this->CreateCommand('CHANGE_THEME');
                    $message = "نوع پوسته را انتخاب نمایید.";
                    $this->SendMessage($message, theme_types_key(), CANCEL);

                }


                $command = (array)$this->GetCommand();

                if (isset($command['command'])) {

                    $this->command = $command['command'];

                    if ($this->CheckCommand('SEND_NEWS')) {

                        $this->CreateNews();
                        $this->ClearCommand();
                        $message = "خبر با موفقیت ثبت شد.";
                        $this->SendMessage($message, main_menu());

                    }

                    if ($this->CheckCommand('SEND_BREAKING_NEWS')) {

                        $this->UpdateCommand($command['id']);
                        $this->CreateCommand('SEND_BREAKING_NEWS_TIME_DISPLAY');
                        $this->SendMessage(SEND_BREAKING_NEWS_TIME_DISPLAY);
                    }

                    if ($this->CheckCommand('SEND_BREAKING_NEWS_TIME_DISPLAY')) {
                        $this->CreateBreakingNews();
                        $this->ClearCommand();
                        $message = "خبرفوری با موفقیت ثبت شد.";
                        $this->SendMessage($message, main_menu());
                    }

                    if ($this->CheckCommand('SEND_BREAKING_NEWS_TIME_DISPLAY')) {

                        $this->CreateBreakingNews();
                        $this->ClearCommand();
                        $message = "خبرفوری با موفقیت ثبت شد.";
                        $this->SendMessage($message, main_menu());

                    }
                }

            }

            $this->MainMessage();

        }

        return new JsonResponse(array(TRUE));
    }


    public function TestApi() {
        $w = $this->GetWatch();
        var_dump(unserialize($w->message));
        die();

        return new JsonResponse(array(TRUE));
    }

    public function GetMobile() {
        apiRequestWebhook("sendMessage", array(
            'chat_id'      => $this->chat_id,
            "text"         => 'برای ادامه، شماره همراه خود را تایید کنید.',
            'reply_markup' => array(
                'keyboard'          => array(
                    array(
                        array(
                            'text'            => "تایید شماره همراه",
                            'request_contact' => TRUE,
                        ),
                    ),
                ),
                'one_time_keyboard' => TRUE,
                'resize_keyboard'   => TRUE,
                'ForceReply'        => TRUE,
            ),
        ));
    }

    public function CheckText($text) {

        if ($this->text === $text) {
            return true;
        }

        return false;
    }

    public function CheckCommand($command) {

        if ($this->command === $command) {
            return true;
        }

        return false;
    }

    public function MainMessage($clear = false) {

        if ($clear) {
            $this->ClearCommand();
        }

        $message = " با استفاده از منوی زیر می توانید از سامانه استفاده نمایید.";
        $this->SendMessage($message, main_menu());

        return true;
    }

    public function SendMessage($message, $options = [], $cancel = false) {

        $request = [
            'chat_id' => $this->chat_id,
            'text'    => $message,
        ];

        if (!isset($options[0]['text'])) {

            if ($cancel) {
                $options[] = CANCEL;
            }

            $request['reply_markup'] = [
                'keyboard'          => array_chunk($options, 2),
                'one_time_keyboard' => TRUE,
                'resize_keyboard'   => TRUE,
            ];
        }
        else {

            if ($cancel) {
                $options[] = [
                    'text'          => CANCEL,
                    'callback_data' => CANCEL,
                ];
            }

            $request['reply_markup'] = [
                'inline_keyboard' => array_chunk($options, 1),
            ];
        }

        apiRequestWebhook("sendMessage", $request);
    }

    public function EditMessage($message, $options = [], $cancel = false) {

        $request = [
            'chat_id'    => $this->chat_id,
            'text'       => $message,
            'message_id' => $this->message_id,
        ];

        if (!isset($options[0]['text'])) {

            if ($cancel) {
                $options[] = CANCEL;
            }

            $request['reply_markup'] = [
                'keyboard'          => array_chunk($options, 2),
                'one_time_keyboard' => TRUE,
                'resize_keyboard'   => TRUE,
            ];
        }
        else {

            if ($cancel) {
                $options[] = [
                    'text'          => CANCEL,
                    'callback_data' => CANCEL,
                ];
            }

            $request['reply_markup'] = [
                'inline_keyboard' => array_chunk($options, 1),
            ];
        }

        apiRequestWebhook("editMessageText", $request);
    }

    public function GetWatch() {
        $query = \Drupal::database()->select('watchbot', 'c');
        $query->fields('c');
        $query->orderBy('c.id', 'DESC');
        $query->range(0, 1);

        return $query->execute()->fetchObject();
    }


    public function UserIsExist() {
        $query = \Drupal::entityQuery('user');
        $result = $query->Condition('field_telegram_id.value', $this->chat_id, '=')->range(0, 1)->execute();

        return $result;
    }

    public function CreateUser($phone_number) {

        $phone_number = Unicode::substr($phone_number, -10);

        $account = user_load_by_mobile($phone_number);

        if ($account) {
            $account->set('field_telegram_id', $this->chat_id);
            $account->save();
        }
        else {

            $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
            $user = \Drupal\user\Entity\User::create();

            //Mandatory settings
            $user->setPassword(user_password(8));
            $user->enforceIsNew();
            //    $user->setEmail('test@test.com');
            $user->setUsername($phone_number); //This username must be unique and accept only a-Z,0-9, - _ @ .

            //Optional settings
            $user->set("init", 'email');
            $user->set("langcode", $language);
            $user->set("preferred_langcode", $language);
            $user->set("preferred_admin_langcode", $language);
            //$user->set("setting_name", 'setting_value');
            $user->activate();

            $user->set('field_telegram_id', $this->chat_id);
            $user->set('field_mobile', $phone_number);

            //Save user
            $user->save();
        }

        $message = "اطلاعات شما به درستی ذخیره شد.منتظر تایید مدیر باشید.";
        $this->SendMessage($telegram_id, $message, ['بررسی وضعیت']);

        return true;
    }

    public function CreateCommand($command) {
        $conn = \Drupal::database();
        $conn->insert('tv_bot_command')->fields(array(
            'command' => $command,
            'user_id' => $this->chat_id,
        ))->execute();

        return TRUE;
    }


    public function watchBot($id, $message) {
        $conn = \Drupal::database();
        $conn->merge('watchbot')->keys(array(
            'id' => $id,
        ))->fields(array(
            'id'      => $id,
            'message' => $message,
        ))->execute();

        return TRUE;
    }


    public function UpdateCommand($command_id, $tid = null) {
        $query = \Drupal::database();
        $query->update('tv_bot_command')->fields(array(
            'answer' => $this->text,
            'tid'    => $tid,
        ))->condition('id', $command_id)->execute();

        return TRUE;
    }

    public function GetCommand() {
        $query = \Drupal::database()->select('tv_bot_command', 'c');
        $query->fields('c');
        $query->condition('c.user_id', $this->chat_id);
        $query->orderBy('c.id', 'DESC');
        $query->range(0, 1);

        return $query->execute()->fetchObject();
    }

    public function GetPreviousCommand() {
        $query = \Drupal::database()->select('tv_bot_command', 'c');
        $query->fields('c');
        //        $query->condition('c.user_id', $this->chat_id);
        $query->orderBy('c.id', 'DESC');
        $query->range(0, 2);

        return end($query->execute()->fetchAll());
    }

    public function CreateContact($text, $user) {
        $node = Node::create(['type' => 'contact']);

        $node->setTitle($text);
        $node->set('body', $text);
        $node->setOwner($user);
        $node->save();

        return TRUE;
    }

    public function CreateNews() {
        $term = Term::create(array(
            'name'              => Unicode::substr($this->text, 0, 200),
            'vid'               => 'news',
            'field_news'        => $this->text,
            'field_news_enable' => true,
        ));
        $term->save();

        return $term->id();
    }

    public function CreateBreakingNews() {
        $pc = $this->GetPreviousCommand();
        $term = Term::create(array(
            'name'                       => Unicode::substr($pc->answer, 0, 200),
            'vid'                        => 'breaking_news',
            'field_breaking_news'        => $pc->answer,
            'field_show_time'            => $this->text,
            'field_breaking_news_enable' => true,
        ));
        $term->save();

        return $term->id();
    }

    public function SetTheme() {

        $db = \Drupal::database();
        $db->insert('tv_theme')->fields(array(
            'title' => 'Telegram',
            'type'  => strtolower($this->data),
            'time'  => REQUEST_TIME,
            'uid'   => $this->account->id(),
        ))->execute();
    }

    public function ClearCommand() {
        $query = \Drupal::database()->delete('tv_bot_command');
        $query->condition('user_id', $this->chat_id)->execute();

        return TRUE;
    }

    public function GetNews() {
        $terms = \Drupal::service('entity_type.manager')->getStorage("taxonomy_term")->loadTree('news');

        $result = array();

        foreach ($terms as $term) {
            $result[$term->tid] = $term->name;
        }

        return $result;
    }

    public function RemoveBreakingNews() {
        $query = \Drupal::entityQuery('taxonomy_term')
            ->condition('vid', 'breaking_news')
            ->condition('field_breaking_news_enable', 1);

        $terms = taxonomy_term_load_multiple($query->execute());

        foreach ($terms as $term) {
            $term->set('field_breaking_news_enable', 0);
            $term->save();
        }

        return true;
    }

    public function RemoveTerm() {
        $term = Term::load($this->data);

        if ($term) {
            $term->delete();
        }

        return true;
    }
}